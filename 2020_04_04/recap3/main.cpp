#include <iostream>

using namespace std;

/**
Se citește un număr natural n urmat de n numere naturale.
Să se afișeze diferența dintre suma numerelor pare și
suma numerelor impare din șir.
*/

int a[1024], na;

void citire()
{
    ///citesc sirul
    cin >> na;
    for(int i = 1; i <= na; i++)
        cin >> a[i];
}

int sumap(int r)
{
    int s = 0;
    for(int i = 1; i <= na; i++)
        if(a[i] % 2 == r)
            s += a[i];
    return s;
}

int main()
{
    citire();
    cout << "pare: " << sumap(0) << endl;
    cout << "impare: " << sumap(1) << endl;
    cout << "diff: " << sumap(0) - sumap(1) << endl;
    return 0;
}
