#include <iostream>
#include <cstdlib>
#include <ctime>
#include <conio.h>
#include <fstream>

using namespace std;

int codare(char c)
{
    if(c == 'r' || c == 'R')
        return 0;
    if(c == 'p' || c == 'P')
        return 1;
    if(c == 's' || c == 'S')
        return 2;
    return -1;
}

void printfile(string f)
{
    ifstream fin( f.c_str() );
    char c;
    while(fin >> noskipws >> c)
        cout << c;
    cout << endl;
}

void afisare_alegere(int x)
{
    cout << endl;
    if(x == 0)
        printfile("rock.asc"); ///cout << "rock"
    else if(x == 1)
        printfile("paper.asc");
    else if(x == 2)
        printfile("scissors.asc");
    cout << endl;
}

int punctaj(int player, int npc)
{
    if(player == npc)
        return 0;
    else if(player == 0 && npc == 2) ///piatra vs foarfeca
        return 1;
    else if(player == 1 && npc == 0) ///hartie vs piatra
        return 1;
    else if(player == 2 && npc == 1) ///foarfeca vs hartie
        return 1;
    else
        return -1;

    /// prescurtare pt castig (3 + player - npc) % 3 == 1

}

int runda(int player, int npc)
{
    if(player == -1)
    {
        cout << "semn gresit" << endl;
        return 0;
    }
    int p = punctaj(player, npc);
    if(p == -1)
        cout  << "ai pierdut runda"<<endl;
    else if(p == 1)
        cout << "ai castigat runda" << endl;
    else
        cout << "egalitate" <<endl;
    return p;
}

int main()
{
    srand(time(0));
    int scor = 0, player, npc;

    while(true)
    {
        cout << "scor: " << scor << endl << endl;
        cout << "alege: (R / P / S) ";
        char aleg;
        aleg = getch();
        system("cls");
        player = codare(aleg);
        npc  = rand() % 3;
        cout << "player: ";
        afisare_alegere(player);
        cout << "calculator: ";
        afisare_alegere(npc);
        scor += runda(player, npc);
    }

    return 0;
}
