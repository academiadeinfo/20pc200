#include <iostream>
#include <conio.h>
using namespace std;

/**
Se citește un număr natural n și n numere naturale.
Afișați câte din ele sunt divizibile cu 3 sau au ultima cifră 3.
*/

int a[128], na;

void citire()
{
    cin >> na;
    for(int i = 1; i <= na; i++)
        cin >> a[i];
}

bool conditie(int x)
{
    /// verific dacă x îndeplinește condiția
    return x % 3 == 0  or  x % 10 == 3;
}

int rezolvare()
{
    ///returnează câte elemente respectă condiția
    int c = 0;
    for(int i = 1; i <= na; i++)
        if( conditie(a[i]) )
            c++;
    return c;
}

///extra: explicatie functie fara parametrii
void poveste()
{
    cout << "A fost o data in codarnia..."<< endl;
    getch();
    cout << "O frumoasa printesa care se chema Ruby." << endl;
    getch();
    cout << "Intr-o zi ea primeste vestea ca in nordul regatului sau capcaunul pehasp a inceput sa faca ravagii..";
    getch();
}

int main()
{
   // poveste();
    citire();
    cout << rezolvare() << endl;
    return 0;
}
