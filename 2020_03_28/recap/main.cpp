#include <iostream>

using namespace std;

int max2 (int x, int y)
{
    if(x > y)
        return x;
    else
        return y;
}

int max4 (int x, int y, int z, int w)
{
    return max2( max2(x, y), max2(z, w) );
}

int main()
{
    cout << max2(9, 6) << endl;
    cout << max2(3, 6) << endl;
    cout << max2(6, 6) << endl;

    int a, b, c, d;
    while(true)
    {
        cin >> a >> b >> c >> d;
        cout << max4(a, b, c, d) << endl;
    }

    return 0;
}
