#include <iostream>

using namespace std;

int minim(int x, int y)
{
    if(x < y)
        return x;
    else
        return y;
}

int suma(int x, int y)
{
    return x + y;
}

int prod(int x, int y)
{
    return x * y;
}

int rest(int x, int y)
{
    return x % y;
}

///functie pentru diferenta a 2 nr
int dif(int x, int y)
{
    return x - y;
}

///functie pentru câtul împărțirii a 2 nr
int cat(int x, int y)
{
    return x / y;
}

int main()
{
    int a, b, c;
    cin >> a >> b >> c;
    ///1. Dublul minimului dintre a și b.
    cout << "1. " << prod(2, minim(a, b)) << endl;

    ///2. Diferenta dintre produsul numerelor a, b, c și suma lor.
    cout << "2. " << dif(prod(a, prod(b, c)), suma(a, suma(b, c))) << endl;

    ///3. Ultima cifră a lui c
    cout << "3. " << rest(c, 10) << endl;

    ///4. Suma ultimeleor cifre ale lui a, b, c
    cout << "4. " << suma(rest(a, 10), suma(rest(b, 10), rest(c, 10))) << endl;

    ///5. Penultima cifră a lui c
    cout << "5. " << rest(cat(c, 10), 10) << endl;

    ///6. Dublul sumei dintre produsul lui a și b și produsul lui b și c
    cout << "6. " << prod(2, suma(prod(a, b), prod(b, c))) << endl;

    ///7. a + b*c - b
    cout << "7. " << dif(suma(a, prod(b, c)), b) << endl;

    ///8. a + b/2 - (a-c)
    cout << "8. " << dif(suma(a, cat(b, 2)), dif(a, c)) << endl;

    return 0;
}
