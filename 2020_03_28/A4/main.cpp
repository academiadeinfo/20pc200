#include <iostream>

using namespace std;

///primește un paramatru întreg x și returnează suma cifrelor lui x
int sumacif(int x)
{
    int s = 0; // declar suma, la început 0
    while(x) // cât timp mai am cifre (x != 0 )
    {
        s += x % 10; //adaug la suma ultima cifra a lui x: s = s+x%10
        x /= 10; // tai ultima cifra a lui x: x = x /10
    }
    return s; ///returnez rezultatul: suma cifrelor.
}

int max2(int x, int y)
{
    if(x > y)
        return x;
    return y;
}

int max2(int x, int y, int z)
{
    return max2(x, max2(y, z));
}

int main()
{
    int a, b, c;
    cin >> a >> b >> c;

    cout << max2(sumacif(a), sumacif(b), sumacif(c));

    return 0;
}
