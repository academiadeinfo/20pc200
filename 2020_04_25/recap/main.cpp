#include <iostream>

using namespace std;


int main()
{
    string s = "super";

    for(int i = 0; i < s.length(); i++)
        cout << s[i] << " ";

    s += " mega";
    cout << endl << s << endl;

    string text;
    getline(cin, text);

    int poz = text.length() / 2;
    text.insert(poz, "-\n");

    cout << endl << text << "\n";

    return 0;
}
