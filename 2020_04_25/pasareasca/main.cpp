#include <iostream>
#include <conio.h>

using namespace std;

///Limba păsărească:
/// după fiecare vocală se pune litera p
/// și se repetă vocala
/// ex: exemplu -> epexepemplupu
///   nu este greu -> nupu epestepe grepeupu
///   text de proba -> tepext depe probapa
///   tepestupul epestepe upusopor -> testul este usor

bool eVocala(char c)
{
    string vocale = "AEIOUaeiou";
    if(vocale.find(c) == -1)
        return false;
    else
        return true;
}

string codare(string original)
{
    string codat = "";

    for(int i = 0; i < original.length(); i++)
    {
        codat += original[i];
        if(eVocala(original[i]))
            codat = codat + "p" + tolower(original[i]);
    }
    return codat;
}

string decodare(string codat)
{
    string s = codat;
    for(int i = 0; i < s.length(); i++)
        if(eVocala(s[i]))
            s.erase(i+1, 2);

    return s;
}
int main()
{
    while(1)
    {
        char op;
        cout << "codare (c) / decodare (d): ";
        op = getch(); ///#include <conio.h>
        cout << "\ntext: ";
        string text;
        getline(cin, text);
        if(op == 'c')
            cout << codare(text) << endl;
        else if(op == 'd')
            cout << decodare(text) << endl;
        else
            cout << "operatie necunoscuta" << endl;
    }
    return 0;
}
