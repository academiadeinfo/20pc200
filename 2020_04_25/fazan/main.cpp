#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>

using namespace std;

string dictRO[2048], dictEN[2048];
int nd = 0; /// nr elem din dict

void citireDictionar()
{
    ifstream f ("dictionar.txt");
    while(f >> dictRO[nd])
    {
        getline(f, dictEN[nd]);
        nd++;
    }
}

string lastWord = "";

bool corect(string cuv)
{
    if(cuv.length() < 3)
        return false;
    if(lastWord == "")
        return true;
    string primele = cuv.substr(0, 2);
    int nlast = lastWord.length();
    string ultimele = lastWord.substr(nlast-2, 2);
    if(primele == ultimele)
        return true;
    else
        return false;
}

string cautaCuv()
{
    string corecte[1000];
    int ncorecte = 0;
    for(int i = 0; i < nd; i++)
    {
        if(corect(dictRO[i]))
            corecte[ncorecte++] = dictRO[i];
    }
    int poz = rand() % ncorecte;
    if(ncorecte > 0)
        return corecte[poz];
    else
        return "-1"; /// nu am gasit nimic
}

int main()
{
    srand(time(NULL));
    citireDictionar();
    int vieti = 5, puncte = 0;
    while(vieti > 0 && puncte < 5)
    {
        cout << "vieti: " << vieti << " puncte: " << puncte << endl;
        cout << "ultimul cuvant: " << lastWord << endl;
        cout << "jucator: ";
        string cuv;
        cin >> cuv;
        if(corect(cuv))
        {
            lastWord = cuv;
            string npc = cautaCuv();
            if(npc == "-1")
            {
                cout << "calculator: m-ai inchis, ai un punct" << endl;
                puncte++;
                lastWord = "";
            }
            else
            {
                cout << "calculator: " << npc << endl;
                lastWord = npc;
            }
        }
        else
        {
            cout << "Gresit, pierzi o viata" << endl;
            vieti--;
            lastWord = "";
        }
    }

    return 0;
}
