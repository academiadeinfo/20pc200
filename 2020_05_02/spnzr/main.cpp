#include <iostream>
#include <cstdlib>
#include <ctime>
#include <conio.h>
#include <fstream>

using namespace std;

/// JOCUL de SPÂNZURĂTOAREA

string cuvant; ///cuvântul corect
string ghicit; ///ce știm până acum
string vechi;  /// literele de până acum
string nume; ///numele jucătorului
int puncte = 0; ///punctajul actual
int vieti; ///numărul de vieți actual
bool castigat; ///jucătorul a câștigat sau nu runda actuală

string dictionar[1024];
int nd = 0;

void citire()
{
    ifstream f ("cuvinte.txt");
    while(f >> dictionar[nd])
        nd++;
}

void inlocuiesc(char lit)
{
    for(int i = 0; i < cuvant.length(); i++)
        if(cuvant[i] == lit)
            ghicit[i] = cuvant[i];
}

void init()
{
    int randi = rand() % nd; /// un număr random de la 0 la nd
    cuvant = dictionar[randi];
    vieti = 5;
    ghicit = "";
    vechi = "";
    castigat = false;
    for(int i = 0; i < cuvant.length(); i++)
        ghicit += "_";
    inlocuiesc(cuvant[0]); ///descopăr prima literă
    int ultpos = cuvant.length() - 1; ///ultima poziție din cuvânt
    inlocuiesc(cuvant[ultpos]); /// descopăr ultima literă
    ///descopar caractere speciale:
    inlocuiesc('-');
    inlocuiesc(' ');
}

void printFile(string fn)
{
    ifstream f (fn.c_str());
    char c;
    while(f >> noskipws >> c)
        cout << c;
    cout << endl << endl;
}

void afis()
{
    system("cls"); ///șterg ecranul
    if(cuvant == ghicit)
        printFile("win.asc");
    else
    {
        string fname = "vieti";
        fname += ('0' + vieti);
        fname += ".asc";
        printFile(fname);

    }
    cout << nume << "  punctaj: " << puncte << endl;
    for(int i = 0; i < ghicit.length(); i++)
        cout << ghicit[i] << " ";
    cout << endl << "pana acum: " << vechi << endl << endl;
}
bool literabuna(char lit)
{
    if(cuvant.find(lit) != -1)
        return true;
    else
        return false;
}
void verificCastig()
{
    if(ghicit == cuvant)
    {
        castigat = true;
        cout << "Bravo, ai ghicit cuvantul, (^_^)" << endl;
    }
    else if(vieti == 0)
        cout << "Oups, ai pierdut (x_X), cuvantul era: " << cuvant << endl;
}
void runda()
{
    init(); ///initalizăm runda
    afis(); ///afișăm ce știm până acum
    while(vieti > 0 && !castigat) ///mai am vieți și nu am câștigat
    {
        char c;
        cout << "introdu litera: ";
        c = getch(); /// cin >> c;
        vechi = vechi + c + " ";
        inlocuiesc(c);
        if(!literabuna(c))
            vieti--;
        afis();
        if(literabuna(c))
            cout << "Bravo, ai ghicit o litera! " << endl;
        else
            cout << "Oups, litera " << c << " nu exista." << endl;
        verificCastig();
    }
}

int main()
{
    srand(time(NULL)); /// initalizăm seedul pentru random
    citire(); ///citim cuvintele din dictionar
    cout << "Salut, cum te cheama? ";
    cin >> nume;
    char maijoc = 'y';
    while(maijoc == 'y' || maijoc == 'Y') //cât timp mai mai vrea să joace
    {
        runda();
        if(castigat)
            puncte++;
        else
        {
            ///TODO: verificat punctajul pentru scorebord
            puncte = 0;
        }
        cout << endl << "doresti sa continui jocul? (y/n)";
        maijoc = getch(); //citesc litera fără să o afișez
    }
    return 0;
}
