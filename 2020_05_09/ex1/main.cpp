#include <iostream>
#include <fstream>

using namespace std;

///din fișierul mat.in se citesc nr n și m
///și o matrice de n linii și m coloane
///se va afișa matricea.

///la infinit: se va citi o linie q și
///    se va afișa câte elemente pare sunt pe linia q

int a[100][100], n, m;

void citire()
{
    ///citim matricea din fișier
    ifstream f ("mat.in");
    f >> n >> m;
    for(int i = 1; i <= n; i++)
        for(int j = 1; j <= m; j++)
            f >> a[i][j];
}

void afisare()
{
    ///afișăm matricea
    for(int i = 1; i <= n; i++, cout << endl)
        for(int j = 1; j <= m; j++)
            cout << a[i][j] << " ";

    cout << endl;
}

///va returna câte elemente pare sunt pe linia lin
int catepare(int lin)
{
    int c = 0;
    for(int j = 1; j <= m; j++)
        if(a[lin][j] % 2 == 0)
            c++;
    return c;
}

int main()
{
    citire();
    afisare();
    while(true) /// la infinit
    {
        int q;
        cout << "linia: ";
        cin >> q;
        cout << "pare: " << catepare(q) << "\n\n";
    }
    return 0;
}
