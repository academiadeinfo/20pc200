#include <iostream>

using namespace std;

int main()
{
    ///1. Declarare
    int a[100][100];
    int na, ma; ///na - nr de linii, ma- nr de col
    cin >> na >> ma; ///citim nr de lii si col.

    ///2. Parcurgerea întregii matrice
    for(int i = 1; i <= na; i++) /// pentru fiecare linie i
    {
        for(int j = 1; j <= ma; j++) ///fiecare col
            a[i][j] = 0; ///linia i coloana j
    }

    ///3. Parcurgerea unei linii:
    int lin = (na+1) / 2; ///aleg linia din mijloc
    for(int j = 1; j <= ma; j++)
        a[lin][j] += 4; ///adaug 4 pe linia lin

    ///4. Parcurgerea unei coloane:
    int col = (ma+1) / 2; ///aleg coloana din mijloc
    for(int i = 1; i <= na; i++)
        a[i][col] += 4; ///adaug 4 pe coloana col

    ///5. Afișarea unei matrice
    for(int i = 1; i <= na; i++)
    {
        for(int j = 1; j <= ma; j++)
            cout << a[i][j] << " ";
        cout << endl;
    }

    return 0;
}
