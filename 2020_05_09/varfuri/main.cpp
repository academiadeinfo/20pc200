#include <iostream>

using namespace std;

int teren[100][100];
int n;

void citire()
{
    ifstream fin ("teren.in");
    fin >> n;
    for(int i = 1; i <= n; i++)
        for(int j = 1; j <= n; j++)
            fin >> teren[i][j];
}

bool eVarf(int x, int y)
{
    return (
        a[x][y] > a[x-1][y  ] && /// sus
        a[x][y] > a[x  ][y+1] && /// dreapta
        a[x][y] > a[x+1][y  ] && /// jos
        a[x][y] > a[x  ][y-1];   /// stanga
    )

}

int main()
{
    citire();
    int contor = 0;
    for(int i = 1; i <= n; i++)
        for(int j = 1; j <= n; j++)
            if(eVarf(i, j))
                contor++;
    cout << "varfuri: " << contor << endl;
    return 0;
}
