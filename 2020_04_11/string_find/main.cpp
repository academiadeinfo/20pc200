#include <iostream>

using namespace std;

int main()
{
    string s;// = "elsa are mere, dar elsa a ramas fara pere. Ce poate face elsa acum?";
    getline(cin, s);
    int poz = s.find('z');
    cout << poz << endl;
    s[poz] = 'M';
    cout << s<< endl;

    poz = s.find("elsa");
    while(poz != -1)
    {
        s[poz] = 'E';
        poz = s.find("elsa");
    }
    cout << s <<endl <<endl;
    ///substring de la pozitia 7 la pozitia 17
    cout << s.substr(7, 10) << endl;

    int start = s.find(','); ///incep de la prima virgula
    int nc = s.find('.') - start; ///numarul de caractere de la start la primul punct
    cout << s.substr(start, nc) << endl;

    return 0;
}
