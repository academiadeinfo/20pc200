#include <iostream>

using namespace std;

int main()
{
    ///se citește o literă, verificați dacă este vocală
    string vocale = "aeiouAEIOU";

    char lit;
    cout << "litera: ";
    cin >> lit;

    if(vocale.find(lit) != -1) ///dacă găsesc litera
        cout << "este vocala" << endl;
    else
        cout << "nu este vocala" << endl;

    ///Provocare: se citeste un cuvant, cate vocale conține.

    return 0;
}
