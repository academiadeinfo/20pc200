#include <iostream>

using namespace std;

int main()
{
    string cuv;
    cin >> cuv;
    cout << "ati introdus: " << cuv << endl;
    cout << "prima litera: " << cuv[0] << endl;
///	.length() -> returnează lungimea stringului (numărul de caractere)
    int ncuv = cuv.length();
    for(int i = 0; i < ncuv; i++)
        cout << cuv[i] << " ";
    cout << endl;
///provocare: se citeste o literă (un char)
///de cate ori apare litera in cuv?
    char lit;
    cout << "litera: ";
    cin >> lit;
    int contor = 0;
    for(int i = 0; i < cuv.length(); i++)
        if(cuv[i] == lit)
            contor++;
    cout << "apare de: " << contor << " ori\n";

    return 0;
}
