#include <iostream>
#include <conio.h>

using namespace std;

int main()
{
    string cuv1, cuv2;
    cin >> cuv1 >> cuv2;

    if(cuv1 > cuv2)
        cout << cuv1 << " > " << cuv2;
    else if(cuv1 < cuv2)
        cout << cuv1 << " < " << cuv2;
    else
        cout << cuv1 << " = " << cuv2;
    cout << endl;

    char c;
    string pass = "";
    c = getch();
    while(c != '\r')
    {
        cout << '*';
        pass += c; ///concatenare-> adaugare caracter la sfarsitul cuvantului
        c = getch();
    }
    cout << endl;
    cout << pass << endl;

    return 0;
}
