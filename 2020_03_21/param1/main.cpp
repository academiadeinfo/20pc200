#include <iostream>

using namespace std;

///funcție care primește 2 parametrii întregi
///și afișează suma lor
void print_sum(int x, int y)
{
    cout << x + y << endl;
}

int main()
{
    int a, b, c;
    cin >> a >> b >> c;

    print_sum(a, b);
    print_sum(b, c);
    print_sum(a, c);

    return 0;
}
