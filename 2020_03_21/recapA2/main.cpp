#include <iostream>
#include <fstream>

using namespace std;

int a[1024], na;

///funcție care citește șirul
void citire()
{
    ifstream fin ("sir.in");
    fin >> na;
    for(int i = 1; i <= na; i++)
        fin >> a[i];
}

void afisare()
{
    for(int i = 1; i <= na; i++)
        cout << a[i] << " ";
    cout << endl;
}

void maxim()
{
    int maxim = 0; //spun ca primul maxim e la inceput 0
    for(int i = 1; i <= na; i++)
        if(a[i] > maxim) //daca un element e mai mare decat maximul de pana acum
            maxim = a[i]; // maximul de pana acum devine acel element
    cout << "maxim: " << maxim << endl; // la sfarsit afisez rezultatul
}

void suma()
{
    int s = 0;
    for(int i = 1; i <= na; i++)
        s += a[i];
    cout << "suma: " << s << endl;
}

void inlocuire()
{
    for(int i = 1; i <= na; i++) //iau fiecare element
        if(a[i] % 2 == 0) //dacă e par
            a[i] = a[i] / 2; //il inlocuiesc
}

int main()
{
    citire();
    afisare();
    suma();
    maxim();
    inlocuire();
    afisare();
    suma();
    maxim();

    return 0;
}
