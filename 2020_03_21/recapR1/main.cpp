#include <iostream>
#include <fstream>

using namespace std;

int a[1024], na;

void citire()
{
    ifstream fin ("recap.in");
    fin >> na;
    for(int i = 1; i <= na; i++)
        fin >> a[i];
}

void afisare()
{
    for(int i = 1; i <= na; i++)
        cout << a[i] << " ";
    cout << endl;
}

void catepare()
{
    int c = 0;
    for(int i = 1; i <= na; i++)
        if(a[i] % 2 == 0)
            c++;
    cout << "pare: " << c << endl;
}

void sumaimpare()
{
    int s = 0;
    for(int i = 1; i <= na; i++)
        if(a[i] % 2 != 0)
            s += a[i];
    cout << "suma impare: " << s << endl;
}

void inlocuire()
{
    for(int i = 1; i <= na; i++)
        a[i] = (a[i] % 10) * (a[i] / 10 % 10);
}

int main()
{
    citire();
    afisare();
    catepare();
    sumaimpare();
    inlocuire();
    afisare();
    catepare();
    sumaimpare();
    return 0;
}
