#include <iostream>

using namespace std;

///Se citesc numerele naturale a și b.
///Câte cifre are a? Dar b?

///funcție care primește un număr întreg
///și returnează numărul lui de cifre
int nrcif(int x)
{
    int c = 0;
    while(x > 0) //cât timp x mai are cifre
    {
        c++; // cresc contorul
        x /= 10; /// x = x / 10 -> tai ultima cifră
    }
    return c; ///returnez rezultatul
}

int main()
{
    int a, b;
    cin >> a >> b;
    cout << "numarul de cifre: "<< endl;
    cout << "a: " << nrcif(a) << endl;
    cout << "b: " << nrcif(b) << endl;

    ///ajutor pentru bonus:
    if(nrcif(a) % 2 == 0)
        cout << "a are nr par de cifre" << endl;

    return 0;
}
