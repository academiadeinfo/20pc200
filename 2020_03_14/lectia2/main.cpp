#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    ifstream fin ("sir.in");

    int a[1024], na;

    fin >> na;
    for(int i = 1; i <= na; i++)
        fin >> a[i];

    int s = 0;
    for(int i = 1; i <= na; i++) //iau fiecare nr din șir
        if(a[i] % 2 == 0) //dacă nr este par
            s += a[i]; //îl adaug la sumă

    cout << "suma pare: " << s << endl;

    return 0;
}
