#include <iostream>
#include <cstdlib>

using namespace std;
///definesc funcția maxim cu 2 parametrii va returna maximul dintre ei
int maxim(int a, int b)
{
    if(a > b) /// dacă a e mai mare decât b
        return a; ///resultatul este a
    else
        return b; ///returnez b
}

int main()
{
    ///apelez funcția cu diferiți parametrii
    cout << maxim(9.5, 9.0) << endl;
    cout << maxim(12, 3) << endl;

    while(1)
    {
        int x, y;
        cin >> x >> y;
        cout << maxim(x, y) << endl;
    }

    return 0;
}
